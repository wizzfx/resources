import sys
import os
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QPushButton, QVBoxLayout, QHBoxLayout, QFrame
from PyQt5.QtCore import Qt, QPoint

class HelloApp(QWidget):
    def __init__(self):
        super().__init__()

        # Set window properties (borderless, transparent, always on top)
        self.setWindowTitle("Transparent Window")
        self.setGeometry(300, 300, 400, 200)

        # Remove window borders and make it transparent
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
        self.setAttribute(Qt.WA_TranslucentBackground)

        # Create main layout
        main_layout = QVBoxLayout()
        main_layout.setContentsMargins(10, 10, 10, 10)

        # Title bar (draggable area)
        self.title_bar = QFrame(self)
        self.title_bar.setFixedHeight(30)
        self.title_bar.setStyleSheet("background-color: rgba(0, 0, 0, 150); border-radius: 10px;")

        # Title bar layout (for text and close button)
        title_layout = QHBoxLayout()
        title_layout.setContentsMargins(5, 5, 5, 5)

        self.title_label = QLabel("My App", self)
        self.title_label.setStyleSheet("color: white; font-size: 16px; background: transparent;")
        title_layout.addWidget(self.title_label)

        # Close button (small icon ❌)
        self.close_button = QPushButton("❌", self)
        self.close_button.setFixedSize(30, 30)
        self.close_button.setStyleSheet("""
            background: transparent; 
            color: white; 
            font-size: 18px;
            border: none;
        """)
        self.close_button.clicked.connect(self.close)
        title_layout.addWidget(self.close_button)

        self.title_bar.setLayout(title_layout)
        main_layout.addWidget(self.title_bar)

        # Horizontal layout for folder buttons
        button_layout = QHBoxLayout()
        button_layout.setSpacing(10)

        # Create multiple buttons for opening folders
        folder_paths = [
            (r"C:\Users\Public\Documents", "Folder 1"),
            (r"C:\Users\Public\Pictures", "Folder 2"),
            (r"C:\Users\Public\Music", "Folder 3")
        ]

        for path, name in folder_paths:
            btn = QPushButton(name)
            btn.setFixedSize(100, 40)
            btn.setStyleSheet("""
                font-size: 16px;
                padding: 5px;
                background-color: rgba(0, 0, 0, 180);
                color: white;
                border-radius: 5px;
            """)
            btn.clicked.connect(lambda checked, p=path: self.open_folder(p))
            button_layout.addWidget(btn)

        main_layout.addLayout(button_layout)

        # Set the layout to the window
        self.setLayout(main_layout)

        # Variables for dragging window
        self.drag_position = None

        # Enable dragging the title bar
        self.title_bar.mousePressEvent = self.mousePressEvent
        self.title_bar.mouseMoveEvent = self.mouseMoveEvent
        self.title_bar.mouseReleaseEvent = self.mouseReleaseEvent

    def open_folder(self, folder_path):
        if os.path.exists(folder_path):
            os.startfile(folder_path)  # Opens the folder in File Explorer
        else:
            print(f"Folder not found: {folder_path}")

    # Enable window dragging from the title bar only
    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.drag_position = event.globalPos() - self.frameGeometry().topLeft()
            self.setCursor(Qt.OpenHandCursor)
            event.accept()

    def mouseMoveEvent(self, event):
        if self.drag_position is not None and event.buttons() == Qt.LeftButton:
            self.move(event.globalPos() - self.drag_position)
            event.accept()

    def mouseReleaseEvent(self, event):
        self.drag_position = None
        self.setCursor(Qt.ArrowCursor)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = HelloApp()
    window.show()
    sys.exit(app.exec_())
