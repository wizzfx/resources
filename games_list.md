# Games List

## city building / economic games
* Industries of Tytan - ( s-f city building game )
* Voxel Tycoon - ( cartoon style city building game )
* Rise of Industry - ( cartoon style city building game )
* Concrete Jungle - ( cartoon style city building game )
* Anno 1800 - ( industrial revolution - game done by Ubisoft )
* Anno 1404 - ( one of best economics games )
* Tropico 4 and 5 ( 4 is also one of the best economics games, 4 is better than 5 )

## strategy
* overland (cartoon, strategy)
* XCOM, XCOM 2
* Re-Legion
* Frostpunk

## horror platformers
* little nightmares
* darq

## other
* Renegade Ops
* Superliminal


## Shooters Top-Down
* Halo Spartan Assault 
* HELLDIVERS 1,2