# Books

* [Tools of Titans](https://toolsoftitans.com) - Timothy Ferriss - the tactics, routines, and habits of billionaires, icons, and world-class performers.

* Fooled by Randomness: The Hidden Role of Chance in Life and in the Markets - Nassim Nicholas Taleb
