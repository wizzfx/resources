import sys
import os
import json
from PyQt6.QtWidgets import (QApplication, QMainWindow, QWidget, QVBoxLayout, QHBoxLayout, 
                             QPushButton, QFileDialog, QScrollArea, QGridLayout, QLabel,
                             QDialog, QMessageBox)
from PyQt6.QtGui import QPixmap, QGuiApplication, QFont
from PyQt6.QtCore import Qt

class ThumbnailLabel(QLabel):
    def __init__(self, pixmap, path):
        super().__init__()
        self.path = path
        self.setPixmap(pixmap)
        self.setAlignment(Qt.AlignmentFlag.AlignCenter)
        self.setScaledContents(True)
        self.setFixedSize(100, 100)
        self.setStyleSheet("border: 2px solid #1E88E5; border-radius: 5px; padding: 5px;")  # Softer blue border with rounded corners

    def mousePressEvent(self, event):
        if event.button() == Qt.MouseButton.LeftButton:
            self.show_full_image()

    def show_full_image(self):
        dialog = FullImageDialog(self.path, self.window())
        dialog.exec()

class FullImageDialog(QDialog):
    def __init__(self, image_path, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Full Image View")

        screen = QGuiApplication.primaryScreen().geometry()
        self.setFixedSize(screen.width() // 2, screen.height() // 2)

        layout = QVBoxLayout()
        
        self.image_label = QLabel()
        self.image_label.setAlignment(Qt.AlignmentFlag.AlignCenter)
        
        self.original_pixmap = QPixmap(image_path)
        self.update_image()
        
        layout.addWidget(self.image_label)
        
        close_button = QPushButton("Close")
        close_button.clicked.connect(self.close)
        close_button.setStyleSheet("background-color: #007BFF; color: white; border-radius: 5px; padding: 10px;")  # Button styling
        layout.addWidget(close_button)
        
        self.setLayout(layout)

    def update_image(self):
        scaled_pixmap = self.original_pixmap.scaled(
            self.image_label.size(),
            Qt.AspectRatioMode.KeepAspectRatio,
            Qt.TransformationMode.SmoothTransformation
        )
        self.image_label.setPixmap(scaled_pixmap)

    def resizeEvent(self, event):
        super().resizeEvent(event)
        self.update_image()

class ImageGallery(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Image Gallery")
        self.setGeometry(100, 100, 800, 600)
        
        # Set a modern font
        font = QFont("Arial", 10)
        QApplication.setFont(font)

        # Set a dark mode background color
        self.setStyleSheet("background-color: #121212; color: #E0E0E0;")  # Dark gray background with light text

        main_widget = QWidget()
        main_layout = QVBoxLayout()

        button_layout = QHBoxLayout()
        load_folder_button = QPushButton("Load Folder")
        load_folder_button.clicked.connect(self.load_folder)
        load_folder_button.setStyleSheet("background-color: #007BFF; color: white; border-radius: 5px; padding: 10px;")
        
        clear_gallery_button = QPushButton("Clear Gallery")
        clear_gallery_button.clicked.connect(self.clear_gallery)
        clear_gallery_button.setStyleSheet("background-color: #FF3D00; color: white; border-radius: 5px; padding: 10px;")  # Clear action in vibrant red
        
        button_layout.addWidget(load_folder_button)
        button_layout.addWidget(clear_gallery_button)
        main_layout.addLayout(button_layout)

        self.scroll_area = QScrollArea()
        self.scroll_area.setWidgetResizable(True)
        self.scroll_content = QWidget()
        self.grid_layout = QGridLayout(self.scroll_content)
        self.scroll_area.setWidget(self.scroll_content)
        main_layout.addWidget(self.scroll_area)

        main_widget.setLayout(main_layout)
        self.setCentralWidget(main_widget)

        self.image_paths = []
        self.load_saved_images()

    def load_saved_images(self):
        if os.path.exists('gallery_data.json'):
            with open('gallery_data.json', 'r') as f:
                self.image_paths = json.load(f)
            self.load_images()

    def save_images(self):
        with open('gallery_data.json', 'w') as f:
            json.dump(self.image_paths, f)

    def load_folder(self):
        folder = QFileDialog.getExistingDirectory(self, "Select Folder")
        if folder:
            for filename in os.listdir(folder):
                if filename.lower().endswith(('.png', '.jpg', '.jpeg', '.bmp', '.gif')):
                    image_path = os.path.join(folder, filename)
                    if image_path not in self.image_paths:
                        self.image_paths.append(image_path)
            self.load_images()
            self.save_images()

    def load_images(self):
        for i in reversed(range(self.grid_layout.count())): 
            self.grid_layout.itemAt(i).widget().setParent(None)

        row = 0
        col = 0
        for image_path in self.image_paths:
            if os.path.exists(image_path):
                pixmap = QPixmap(image_path)
                thumbnail = ThumbnailLabel(pixmap, image_path)
                self.grid_layout.addWidget(thumbnail, row, col)
                
                col += 1
                if col > 5:
                    col = 0
                    row += 1
            else:
                self.image_paths.remove(image_path)

        self.save_images()

    def clear_gallery(self):
        reply = QMessageBox.question(self, 'Clear Gallery', 
                                     "Are you sure you want to clear the gallery?",
                                     QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No, 
                                     QMessageBox.StandardButton.No)
        if reply == QMessageBox.StandardButton.Yes:
            self.image_paths = []
            self.save_images()
            self.load_images()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = ImageGallery()
    window.show()
    sys.exit(app.exec())
