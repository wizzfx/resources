# Unreal Cheets

* stat gpu
* streaming pool 
* Profile GPU 1  (1 and 0 for on/off) /// GPU only
* Unreal Insights /// memory , cpu, etc 
* unit fps
* unit stats
* stat scenerendering
* nanitestats ( nanitestats off )
* r.Shadow.Virtual.ShowStats = "1"
* 16.66ms = 60fps / 33.33ms = 30fps

### performance tricks
* Cooking ( Game-Asset Manager) - specify Maps to Cook instead of cooking everything.
* WPO disable at certain distance ( use it for foliage at large distance ). 
* division in shader is 10-20times slower than multiplication.
* cull distance volume
* draw distance for assets 
* scalability settings
* material instances
* less meshes on the scene (you can combine some of them in one asset for example)
* texture streaming
* experiment with AA methods (TSR for example can be expensive)
* nanite tassellation off for better performance.

### PRB RIG Lighting
* Material with HDRI plugged into Emissive
* Skylight using same Cubemap
* you can use HDRI image as Cubemap if you save it as Radiance HDR. If you use .exr it is just HDRI not Cubemap.
* HDRI can be altered by sunlight luminance ( there are function for this) for more accurate look.
