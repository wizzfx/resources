# Maya Cheets

### Names changing, prefixe, suffixe
* Modify -> Search Replace Options ( in 'search for' $ to add suffix to name )
* Modify -> Prefix Hierarchy Names
* if you want remove something from name just rename this part to: ''
* cometRename plugin

### Change Instance To Objects
* Modify -> Convert -> Instance To Object

### Vertex reordering tool maya, vertex transfer tool ( since maya 2017 )
* Edit Mesh -> Reorder Vertices
* Mesh -> Transfer Vertex Order

### UV Grid Setup
* UV Editor -> View -> Grid
* UV Grid setup in units  ( Length: 1, Gid line every: 0.25, Subdivision: 8 )

### Vertex Weighted Normals
* migNormalTool (migNormalTools.mel)
* AMTNormalsLT (automatic face weighted normals)

### Multiple Objects Attributes
* Attribute Spread Sheet

### Maya Legacy Render Layers
* Preferences -> Redering -> Prefered Render Setup System -> Legacy Render Layers
* ( this will give you access to legacy render layers tabs )

### Outliner Tricks
* Shift click to expand all nodes

### Maya Retopo And Remesh 
* remesh and retopo commands (new in maya 2019)

### Maya Animation Cheets
* alt+V play animation on timeline
* alt + . ( dot ) move frame by frame animation on timeline

### Maya Painting Geometry Strokes
* Paint Effects - Used for painting geometry like brain folds
* Windows -> General Editors -> ContentBrowser -> PaintEffects

### Maya Edge Selection For Reducing Polycount
* Polygon Selection Constraint on Edges

# Photoshop Cheets

### Some tools
* Windows -> Extensions -> Adobe Color Themes ( color wheel etc. )

### Default Normal Map Color
* (128,128,255)

### Using Blend If
* Create adjustment layer ( solid color ), Go to Blending Options for this layer,  
  Use Blend If, hold ALT and left mouse button to separate sliders.

# Zbrush Cheets

### Hard Surface stuff
* Chisel Brush (3D Alpha, Vector Displacement). 
* Multi Alpha Brush (Only 2D Alpha Inside)
* Insert Brushes
* Ctrl+W (Create PolyGroup)
* Shift+E (Show PolyGroup )
* Live Boolean
* Deformation -> Polish By Features (Polish By Separate PolyGroups)
* ZModeler Brush - for polymodeling

### Spotlight
* Brush -> Spotlight Projection - Disable it if you want to sculpt and have references on your spotlight.

* [Live Boolean Zbrush Explained](https://www.youtube.com/watch?v=t_ufMDUGvRs) - Arimus 3D YT channel
* [ZBrush Mech Helmet - Michael Pavlovich](https://www.youtube.com/watch?v=KdLPsEvKLas) - Michael Pavlovich cool hard surface YT channel
* [Nice Hard Surface Timelapse](https://www.artstation.com/artwork/zAXaDQ) - Nikolay Demencevich

### Megascans

* rebuild the NOREngine to make megascans plugin work otherwise after installing plugin the engine won't start.

