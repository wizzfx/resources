# Quotes

* "Whatever you focus on grows"
* "Show me your friends I will show you your future"
* "If you don't ask you don't get"
* "Intensity is the price of excellence"
* "If you think money can’t buy happiness, you don’t know where to shop"
* "It's never and easy time to make a hard decision!"
* "Don't think about it be about it"
* "It always seams impossible untill it's done"

### Benjamin Franklin
* "By failing to prepare, you are preparing to fail."

### Dan Peña
* "Focus on few not the many"
* "You've been engaged in self sabotaging activities for so long it's become a way of life for you"
* "Your kids and employees don't do  what you tell them to do, they do what they see you do"
* "There are work life choices and their consequences"

### Carnegie Institute of Technology
* "85% of your financial success is due to your personality and ability to communicate, negotiate, and lead. Shockingly, only 15% is due to technical knowledge."
