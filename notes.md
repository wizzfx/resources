# Resources

## Concept Art

### Companies
* [http://onepixelbrush.com](http://onepixelbrush.com) - One Pixel Brush Studio
* [http://www.volta.ca](http://www.volta.ca) - Volta
* [https://www.artwaystudio.net](https://www.artwaystudio.net )- Art Way Studio ( concept art & illustrations )

### Websites
* [http://conceptartworld.com](http://conceptartworld.com) - Concept Art World

### People
* [https://www.artstation.com/okon](https://www.artstation.com/okon) - Marek Okon
* [https://www.artstation.com/viag](https://www.artstation.com/viag) - Nicolas Ferrand

## Git

* Basic writing and formatting syntax - [https://help.github.com/articles/basic-writing-and-formatting-syntax](https://help.github.com/articles/basic-writing-and-formatting-syntax)

## Game Dev Companies / Studios
* Electronic Arts Motive - [https://www.ea.com/studios/motive](https://www.ea.com/studios/motive)

## Useful Software
* [XnView](https://www.xnview.com) - Image Viewer

## Learning Platforms ( can publish tutorials )
* [https://newkajabi.com](https://newkajabi.com) - Kajabi

* stowarzyszenie polskie gry - http://polskiegry.eu

# temp


destruction projectile https://www.youtube.com/watch?v=oQf22-B4uzg

physics chain/rope https://www.youtube.com/watch?v=Fqp4Lh2FF0M

state machine and animation blueprint tricks https://www.youtube.com/watch?v=6aZbnldZjeM



# Lighting Tutorial Progress ( Notes )
* Cube map setup ( Texture has vertex node attached to uvs ? )
* Sun light position matches sun position on the cubemap ( HDRI )
* Remember building reflection captures.

* Even if scene is completely overcast ( wether where is no sun, no directional light) might 
be good to add a little bit directional light for better shapes read.

* while making LUT in Photoshop use few screenshots from different places of the scene
to make sure your LUT changes are ok for the whole scene not only for one shot. You can use 
4 or 6 screenshots in one photoshop document.

* using spotlight as fill lights ( didn't have source, used just for fill )
* using spot lights as rim lights

* project settings -> rendering -> auto exposure - (you can disable auto exposure here instead of turning it off in post process volume)

*w3 l10


# Some Task Notes Ideas
* skin for dinosaurs sculpting and painting - also check some tutorials 

# Investments To Do
* spółki dywidendowe obczaić ( asseco, dom developmentm gpw, zywiec etc. )
* [https://strefainwestorow.pl/artykuly...](https://strefainwestorow.pl/artykuly/dywidendy/20200217/spolki-dywidendowe-trend-wzrostowy)
* VNQI Vanguard ETF obczaić gdzie kupować - ETF na REITY.
* Gazprom - dobra dywidenda od wielu lat.

* HL Alyx Gameplay Notes

# Horror Games
* The Impatient
* Outlast
* DeadSpace
* Prodeus

* call of duty zombie mode to check. ( zombies variaties )
* knife comboses ( find maybe animations for hand movements for reference ? )

 * FABRIK node to keeping left hand on weapon
 * IK Two Bone for moving right hand ( might be usefull for sway movement )
 * - IMPORTANT! Locking and unlocking bones while painting weights to avoide selecting wrong bones
   - Selecting Vertex and Bone you can check weight value that this vertex has.
   - Add Influcences option - if you add new bone to skeleton you can add influence to add weights to this bone
     ( to add them to the skin )

     https://www.pond5.com/sound-effects/item/79197858-scary-hit-6d
     https://www.pond5.com/sound-effects/item/92835276-jump-scare-impact-5



* note: hand FOV and weapon FOV works ( weapon clipping or disapearing near walls was solved by adjusting BOUNDING BOX size for weapon ). Still not sure about shadows. Weapon/Hands with FOV adjustment can't cast dynamic shadows.( maybe a separate object will be ok for shadows rendering.) They have to be set to Self Shadow Only.

* note: combos might work only with fov set to 90. Otherwise look weird with hands on different fov

# Still need to do or think about it

* - rain drops on screen fix for multiple res ( falling water )
* - zombie reskin.
* - floor higher subdivision for better water on the floor + water smooth transition
* - ( test brain elements to spawn ).
* - check games with zombies for references
* - zombie neck weird after hit head. ( needs better skinning zombie )
* - (partially fixed)recoil fix for smg while center aim ( must be lower than when aiming base + smooth transitions )
* - shotgun walk animation + shoting aim center animation.
* - zombie cloth, mask
* - zombie takedown animations.
* - ammo box collisions for bullets etc.
* - use r.Tonemapper Sharpen 1
* - small fix - zombie skining + blood on body
* - small fix - water on the floor arena

* - walking animations for weapons.
* - hand ik position for soldier

# SPRINT After Demo 01

# == Known BUGS etc.

* - ragdoll animation sometimes freezes in weird position ( check pose animation asset etc. )
* - finisher wrong position while performed on the slope ( currently only works on flat surface ).
* - No legs IK on the slopes.

high priority

* - metahuman guy assign to ue4 skeleton and bring to ue4

* - dialogues for level ( incoming voice indicator or something ).
* - ammo collecting tweaks for ammo pool.
* - finisher from any weapon ( knife finisher ).
* - loop background attack sound

* - Holo after power attack/jump is waiting to long to act again ( need to cut the end of the jump animation a little ).
spawn ammo settings in main settings
* - holo hit with electricity during power attack. weird behaviour
* - electro hit reinventing ( laser gun or hand electro hit more like spell )



* - smooth smg camera shake
* - numbers fixing ( correcting calculation especially for end hit )
* - check ammo max values ( pool or real magazine , and what to do after collecting ammo ( remove currect magazine or add to pool )).





low priority

* - slide when crouching during run
* - gloves
* - no aim shoot for grenade launcher
* - grenade launcher switch for heavy gun
* - items in equipment box are moving a little when box is opening.
* - getting into cover that is higher above the ground.
* - BUG - throwing grenade while equiping new weapon ( make incorect switinch weapon behaviour ) Temporarily Solved by not allowing to Throw Grenade while Switching Weapons.
* - BUG - when auto collecting enrgy ammo and using electricity hit electro amount is not updating ( not increasing by collected amount ). Some kind of fixed but not sure if electricity reduction is now feeling good.
* - Object interaction is interupted by grenade throwing ( done on purpose so we don't have glitches when throwing grenade
and doing object interaction, because this cause incorrect behaviours like weird weapon equiped/hidden hands etc. )


setting ammo and equipment from gameplay manager
1. collecting ammo ( research , wolfenstein etc)
2. IK for Enemies legs
3. Better AI using EQS 
5. Outdor level test.
6. Better new explosions for barrels and grenades. (sounds?)
7. ammo fixes ( gathering ammo , reloading etc. ) COD research, Doom research.


- finish shotgun mesh

- balance na nowym levelu ( reduce shooting accuracy for soldiers )
- add drop ammo
- smg hand


- launcher bullet, smoke ( need switching system from fire to launcher )
- ammo fix ( getting ammo when gun is not realoaded ).
- back spawner fix ( multiple enemies are spawning at one position/one spawner, one after another )
- concrete sfx / foot sound fix


- electricity hit soldier when in Cover ( need to exit cover )
- done / machine gun modeling
- smg modeling
- shotgun modeling


- steelmill lighting
- if you start changing weapon during transition to aim center crosshair will not fade out.
- holo moves after being hit by electricity
- biggy all around check 30min
- biggy moving out of cage functionality needed.
- more basemeshes for level 2h ( barriers, gates, etc )

nie wygrywa ten który wyzej skacze lecz ten który lepiej działa na polach ( social negotiation, alliance, manipulation ).
doom like mode ( settings )
character speed: 500 ( doom like speed: 800)
footsteps stepsdistance: 220 ( doom like stepsdistance: 350 )

# week 05


- soldierbasic ( add ability for death animations ).
- parameters for tweaking different settings
- electric gun ( add )
- basic soldier ( no machine, human ( check animation that it can use ).)

- wiecej przestrzeni na levelu + wiecej przeciwników z tokenem.
- enemies look research
- fist fighting boss ( quick moving fighting towards player position. power jump attack. etc.)

- level looks research ( cod zombie etc )
- size of level research etc. 


- door spawners ( if spawning not possible in other areas and for begin spawners).
- spawning on the back // partially done, needs debuging



# holo to do
* - new animations tests ( idle must match attack mele animations otherwise weird looking after performing mele ).
* - AI fixing/updating ( strafing, eqs, keeping distance from each other )
* - investigate player detection ( holo )
* - constant poawer attack ability detection
* - enemy anim rotation system towards player ( for steel also )
* - strafing around for AI ( lock on player ).
* - rotation to player after power hit ( try montages with root motion )

# biggy to do
* - make base mesh on new skeleton
* - mele version ( needs attack and move to strafe and attack again ).
* - steel stop anim functionality.

# soldier
* - fix shooting muzzle showing sometimes when it's no longer shooting.

basic zombies + variatons
soldiers ( basic shooting )


steel fihhting boss + ability to shoot + ability to perform jump attack
kalair based quick enemy
smashing blocks ( mozna je wykorzystac, uruchomic je aby pomagały zwalczac enemisów w przejsicu/gatecie etc.)

* - biggy fix shooting ( wider + hands correct position , check target delay )
* - biggy AI ( set min target location to move so it can't pick a spot to close to current and 
also set max distance so it can't be bigger than follow distance ).


* - electric gun is like microwave beem from DOOM eterna OR lightning gun ( not sure if here we can blow up enemy )
* - Design an Research ( enemies and unique features for FPS ).
    - gravity tricks like in CONTROL
    - enemy laser ( laser gun separate ).
    - moving platforms
    - lift 
    - bending enemies ( wheel ).
    - hacking feature.
    - wall running ( titanfall like )
    - heavy boss. ( bosses mele fight abilities ).
        supersoldat wolfenstein
        laser soldat
        hammer soldat
        diesel soldat ( quick moving soldier )
        uber soldat
        rockets launcher ( stand alone or on soldier )
        
call of duty zombie research enemies.

        
* - ladder HUD fix ( some hud elements dont disaper correctly. )
* - Soldier shooting big to fix ( probably after exiting cover something is wrong ).

* - smashing blocks ( + zombie explosion basic implemented ).
* - still mill environment ( lava assets etc + basic meshes or area sketch )
* - covers layout etc.
* - zombie electrical finish feature + explosion ( 
  fix cloth moving after explosion. you need to destroy all zombiemeshes. )
* - fire up zombies ( flame thrower ? )
* - one boss fight ( check animations for it, bossy animation pack ( free on marketplace ) + animation from paragon characters that might be usefull ).
* - steel coffin level.
* - soldiers need to work on different heights, there are some bugs when they are higher than player or lower.
* - AI jumping from height functionality.
https://www.youtube.com/c/BlackDragonStudiosLtd/videos
https://www.youtube.com/c/ReubenWardTutorials/videos perforce etc tutorial setup
* - environment elements to destroy enemies.
* - check new VFX pack for blood that was downloaded already.
* - foot planting for characters so we can make non flat floors. POWER IK
* - niagara skeletal mesh particle emitting
* - soldiers jumping ability
* - patroling feature review for zombies and soldiers etc.


# settings menu needed

* - brightness
* - movement settings

Done
* - DONE - stop montages for nicer character stoping.
* - DONE - trace from mele fixes for high and lower stuff ( eliminates miss when palyer is crouching )
* - FIXED - energy pickup functionality
* - DONE - screen drop switch ON/OFF
* - DONE - triggers for enemies spawning ( during biggy fighting ) ( needs more ballancing. )
* - DONE - health damage screen checkes
* - DONE - cage back doors ( add )
* - FIXED ( only when same icon for all weapons ) crosshair not hiding when start aim center while still switching weapon.
* - DONE - shotgun numbers are showing only one bullet NR ( or acually all but overlapped ) Now they are added together on one.
* - FIXED - automatic fire firing when throwing grenade and keeping fire button pressed.
* - DONE - grenade debuing throwing
* - DONE - red ammo numbers when low
* - DONE throwing grenade ( hand ).
* - FIXED - grenade not damaging player
* - DONE - auto items collection.
* - DONE - array for collected items informations lines.
* - FIXED - Shooting From Cover ( shooting starts when enemy getting up animation is finished. Before it looked like enemy is shooting while still in crouching or transitioning to up position ) Now shooting signal comes from AnimBP_Soldier.
* - DONE - debug old level
* - DONE - can't be damages when In Finisher.
* - FIXED - stop shooting when dead
* - FIXED - hiding weapon after death ( while switching weapons )
* - energy and health icons fix
* - numbers function for different amount of damage.
* - picking up ammo for different weapons
* - shooting to fix
* - FIXEDbiggy is not spawning at cage id 9
* - FIXED ( clamped ) energy drops to -1 ( set so 0 can be the lowest )
* - automatic shoot fix ( for holding fire button )
* - add explosion to holo.
* - spawning numbers on hit.
* - movement mele attack fix ( behaviour tree research )
* - bullet new 
* - slotting ( token system )
* - steel sight 
* - Done use spawners ID to spawn enemies in specyfic places ONLY.
* - Done - temp - motion blur during dash ( probably need some screen directional blur for this ).
* - DONE - Ladder fix needed. ( when switching weapon and entering ladder weapon is not hiding ).
* - FIXED - Shooting from cover at higher position than player ( sphere trace was starting from too low position during crouch )
 ( that was quick fix not final solution ).
* - FIXED - Dashing ability
* - DONE - dirty hands, new knife and new pistol.( pistol si-fi but worn and dirty? )
* - FIXED ( lower acceptance radius + uncheck 'stop on overlay' ) cover investigation, position in cover low.
* - potential smg for modeling ( find )
* - zombie brain mesh element needs dissolve effect.
* - FIXED - knife collision on build. ( dont use names for damage causer BP_Knife )
* - blood on zombies update
* - shotgun reload fixes
* - back knife takedown.
* - shotgun model ( done but will need some adjustments )
* - FIXED - SOLDIER in cover needs to check if player can see him.
* - DONE - threat indicator.
* - FIXED - there are some bugs in sprinting and finisher ( some issues occure after finisher but this needs to be investigated ).
* - soldier shooting target position. ( if player is in cover target should be higher ). slolved by using sphere trace size 20 rather than line trace.
* - important icon visibility fixed.
* - fixed zombie spine bending ( after beeing hit ) during finisher so it remain straight position.
* - fix zombie sounds
* - weapon pistol.
* - brain particles new function in BP instead of particles.
* - Separate sway values for each weapon
* - (done) recoding for "starting wave when we pick up a weapon from equipment spawner"
* - (done) hooks, chains
* - spine rotation points for camera rotation. ( not important, we are not using spine for rotation, hands are separate and arms are attached directly to camera ).
* - FOV research ( min and max values ) 70min-110max ( weapon 77 currently or more. camera 90 )
* -  pt8 Alaris - weapon current ( check if dark weapon will work, maybe see what guns was used in Resident evil or other games ). 
* - ZEV OZ9C (part of glock) 
* - round door collisions
* - resolution
* - zombie eye when not emissive
* - better death screen with option to restart level
* - sound mixes setup
* - low lying fog
* - quality settings, textures etc.
* - audio settings
* - mouse settings
* - zombie blood ( better blending with body )
* - FOV camera environment
* - implement fov sliders and fov plugin ( done )
* - experiment with weapon sway
* - new skeleton
* - hands for new skeleton


# AI stuff
* - from astronauts ( enemies importance indicators ).

# ui templates
* - last of us, cod ww2 , titanfall.

# ideas for full gameplay
* - explosions, using explosives like barrels etc to create a way to escape.
* - flying robots ( drones )
* - interesting environment ( industrial type from wolfenstein ( space base ? )).
* - laser weapon
* - freazing weapon
* - hacking robotów
* - check ghost runner all bosses
* - mad kid idea game


* - emocjonalny związek z wysokością zarobków. Im wyższe tym bardziej się ludziom nie chce pracować bo nie muszą.


# publishers

1 gaming factory
2 movie games
3 Games Operators
4 forever entertainment
5 varsav 
6 gamesincubator
7 hyperstrange

# symilar projects out or in development
* - Ripout  ( enemies can merg with each other )
* - INCISION very indie project.
* - Project Warlock II
* - chains of fury ( gaming factory )
* - Evil Dead 
* - Project Brutality 3.0
* - Vendettas House 2 ( map from call of duty: black ops 3 )

# youtube channel interesting
* - GVMERS 
* - AI and Games


team17 x 
1C x
heads up ?
Humble Bundle ?
tinny build ?
focus home interactive ? 
505 Games ?
PlayWay ?
Devolver Digital ?

Current Tasks:
House Flipper Research 
- barriers ( need new class ? )
- stairs ( probably new class )
- vertex offset for building mesh


high priority:

loading game from different game slots.
game menus
building menu
spawning building from saved game through construction script

cloth wind
roof7
ganek
windows opening

saving material change
proper menus for painting and upgrading

# KNOWN BUGS
- exit and save don't save current building progress.
- rotate tool ( needs to check before rotation to avoid incorrect positions ).
- wall placable object can be placed on ladder ( because ladder is wall type, change it to other type but keep with same functions as wall has ).
- fix collision for walls ( So placable wall object can be placed correctly ).
- door when opens don't check if something is not going to block it( door might go through another door on other wall ).
- some windows can't be rotated.

# one beach finalized

- water overall tweaks
- grass and ground bushes
- textures changes etc.
- 

- sand blend
- sand wet area
- water small edge waves

- trees tweaks
- bark palm tree ( update to new )
- underwater elements meshing
- sky clouds tests

# all bamboo elements

- research how shop looks like and menus
- game menu ( building list, blueprints, stats, buy/sell)
- half wall ?
- furniture research ( add bed, armchair, table? )
  - sofa, shelfes , small comodas. 
- bamboo texture enhancement + wires that are connecting pillars

- wooden barriers ( sticks with rope )
- half roofs fixes
- new fundament wooden rounded


# code stuff X

///
- displacement test agin in ue5 ( still not ready )
- corners walls functionality ( big stuff) ( failed for now, replaced with pillars that are not used currently ) 
- wall corner detection update
///


1. island
- palm trees
- morning glory
- rocks
- bushes
- leafs decals, palm leafs decals.
- shore dead grass on the beach.
- water fixes
2. inside elements
- carpet
- bed double
- small furniture
- sofa
- DONE, wall paintings
3. roofs
4. walls functionalities and textures.

5. other stuff
- icons
- code updates
- tutorial introduction.



- small tropical island reference 
- wall bamboo textured
- window wall without sloped pillar inside.
- roof element for all around stuff.

- paint upgrades ( costs and values ).
- calcualte upgrade costs and add to property value ( research applying upgrades and paints ( house flipper etc. ))
- requirements details in building panel
- roof textures check ( textures com.)

- experiment with lighting ( based on california Red Forest Settings Project ).
- names fixes for objects and upgrades costs
- ! roof color upgrades
- ! roof connection ( half roof and roof )
- blueprint houses.

- separate socket for openwall attached objects
- correcting transform for object when starting to move.
- fix object moving that have other object placed on them.

- add small shelf.
- add wall pictures
- localized grid settings for floor ( if putting object on floor ( not landscape ) with grid settings we should use grid acording to this floor not to world settings ).

- upgrade menu look
- general CG, UI, 3D design.

- 3 beds
- dialogs first tests

- search feature

- wall with horizontal bamboos
- wall small bamboo checker pattern. 
- flat bamboo pattern. 
- roof windows 
- walls rotation checker, sloped walls ( check if rotation is possible , if we don't overlay anything ).

- some main menu fixes ( save and exit is not saving current progress ).

- graphics settings fix loading correct settings to menu
- building area ( blocking )
- marketplace functionality

- bind signature etc
- picking up new map menu for new gameplay
- ground gravel painting 
- ground wooden sidewalk

- pool 


# UI
- UI updates ( finding interesting design + functionality )
- game logo

DONE
- DONE - basic wall sides ( Corners ) detection system ( part 1).
- DONE - foundation sides system completed. 
- DONE - roof bottom fixed mapping for all roof elements.
- DONE - short roof upgrade don't remove money and don't add to property worth.
- DONE - add round selection toolbox
- FIXED - ladder can't place on second floor because of bad collision.
- DONE - fix placable object so it can be placed on different shelfes ( use placable wall object ).
- DONE - placable objects allow to put anywhere where the floor setting is on and collisions are correct. ( put on multiple shelfes needs fixes ).( use wall placable object settings for this ).
- DONE - wall placed objects 
- FIXED - wall placable object delate close objects after being delated.
- DONE - upgrade paint menu for shape upgrades
- DONE - introduce upgrades colors.
- DONE - glass windows ( with also opening functionality )
- DONE - put object on trinagle floor ( fixed by recreating triangle foundation from scrach ).
- DONE - ability to connect half roof to walls
- DONE - 4side door fixes ( moved to attached object ).
- DONE - ! window for wall ( make it to be attached object ). glass windows for wall ( need only fixes - use only on empty wall ).
- DONE - ( added barrier as and Modular upgrade to frame ) barrier to wall construction ( figure out if can be added ).
- DONE - roofs additional length fixes
- DONE - fix move tool ( materials setup is destroyed while moving ).
- DONE - window fabric ( check if other object is blocking, now it can be added even on window )
- DONE - sroda - compile and check if material instances are saving correctly.
- DONE ( new material apply code ) - Create Material Instance At spawn for objects ( to be able to apply current paint upgrade ) + paint upgrade for other static    
       meshes inside of BP ( override default settings )
- FIX - triangle fundament, pillar position.
- DONE - texture upgrades ( or rather full material ),upgrades icons,empty button
- DONE - upgrades ( highlight currently used upgrade, don't allow to apply )
- Done - table,armchair, singlebed
- DONE - paint for construction
- DONE - 4wing door 
- DONE - roof collision modification ( for better walking on the attic, and ability to add stairs (( now it's blocked ))
- DONE - add empty upgrade buttons
- DONE - ladder functionality ( location detection )
- DONE - rotation saving for doors not working
- DONE - crouching
- DONE - make 'allow move' settings ( not all objects shoudl be able to move )
- DONE - stairs bamboo simple
- FIX - footsteps separate for SFX audio controls
- DONE - rotate floor texture ( planks )
- DONE - windows fixes
- DONE - question quit game
- DONE - getting money back on object destroy
- outline FIX - for object not visible after completing MOVE or Build while aiming at same target building object.
- mouse controls done
- graphic settings done
- DONE - fix for object can't be build when player was stanting on initial object position.
- DONE - fix moving CANCEL 
- FIXED - bug ( while building and hovering over other objects- outline for object is blinking while it should be not visible ).
- FIXED - check moving one object after another, looks liek old moved object is remembered during second object moving.
- FIXED - fix after adding another floor and moving object that was one the floor it disapear after placing.
- DONE - fix collision while moving objects and during building ( deactivate collision after building instead of delate ).
- (move and rotate) picking objects and positioning them
- outline debug 
- outline for update,remove,rotate mode.
- continue game should not appear if we don't have save 'game'
- caustics water (fake) https://www.youtube.com/watch?v=9z6EMsoqLDY
- building cost calculating
- roof upgrades ( sides )
- bamboo fundaments
- buildings list ( display estimated worth )
- getting building value for buildings list
- getting money back when removing object
- saving scene to save slot from menu
- windows fix
- roof triangle
- saving one building and one map to one game save one slot.
- temp fix - Shadows Leaking 
- plants and trees placement
- Was already In Place "" placing smaller objects / furnitures etc. 
- interactive objects fixes ( windows rollets )
- color upgrades
- loading scene from save menu
