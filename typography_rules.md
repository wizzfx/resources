# Typography rules  ( 33 rules )



### How to Format a Document

###### 1. Software will make default choices
- Typeface, Type Size, Line Spacing, Margin Sizes, Text Alignment.
- You should probably change all above to make your documents more interesting.

###### 2. Ensure Good Contrast Between Text and Background
- make high contrast, it's easier to read.
- on black background use rather Sans Serif fonts.
- Thin Serif strokes can blend with the background and it's not good because font is unclear, unpleasant to read.
- don't use text on pattern backgrounds it reduces legibility (czytelność).

###### 3. Avoid Chartjunk and Pagejunk
- thinner borders , lighter color, simple design.
- minimize amount of different things on the page.
- make charts more clear, more meaningful to help focus on content.
- every element on the page should add or clarify meaning.
- use whitespaces to make visual breaks.

###### 4. Enforce Consistent Style Within a Document
- looks organized, and makes effective document.
- helps to focus on content.
- make decisions about Typefaces, Type Size, Text Alignments, Leading.

###### 5. Maintaining a Visual Hierarchy
- helps to define a document flow.
- make a document's structure clear to readers.

###### 6. Group Related Page Elements
- related elements should be physically closer together.
- non related elements should be separated.



### How to Format a Large Bodies of Text

###### 7.

###### 8. Set Body Text 2 to 3 Alphabets Wide
- two wide blocks of text are difficult to read.
- 

### How to Choose Typeface

###### 31. Limit Typeface to 2 per Document
- choose 2 typefaces, you can choose more if you are experience but be careful.
- otherwise to many typefaces can make your doc noisy and unprofessional.
- choose different typefaces to create a nice contrast between them.

###### 32. Use Typefaces That Reinforce a Document's Mood

###### 33. Choose Serif or Sans Serif Based on Aesthetics
- Usually Sans Serif are easier to read on the screen (they have simple character shapes).
- Usually Serif is used for printed text.
- Don't use Serif font with thin strokes with black (dark) backgrounds.
- Serif is more traditional style, Sans Serif is more modern, contemporary.
- Choose Typeface That: are legible, aesthetically pleasing, reinforce mood of the document.
- Legibility of Text:
  - size
  - contrast between background
  - x-Height
  - letter spacing
  - stroke width
