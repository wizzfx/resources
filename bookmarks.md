# Bookmarks

### UE4 Tutorials
* How To Slide Characters, Blueprints - [https://www.youtube.com/watch?v=M_FTm1fsll0](https://www.youtube.com/watch?v=M_FTm1fsll0) 

* RTS Blueprint Series - [https://www.youtube.com/watch?v=FZK5T-vAVFA&list=PLDnygpcOYwFW2XtNyiandrLDG__OAZs7Q&index=1](https://www.youtube.com/watch?v=FZK5T-vAVFA&list=PLDnygpcOYwFW2XtNyiandrLDG__OAZs7Q&index=1) 

* Packt Publishing – Mastering Unreal Engine 4.x Game Development 

### Interesting Websites

* nofluffjobs.com/blog - [https://nofluffjobs.com/blog](https://nofluffjobs.com/blog) 

### C++ Tutorials
* (kanał YT: Pasja informatyki) Kurs C++ odc. 0: Programowanie w C++ - [https://www.youtube.com/watch?v=ErOzmh3BiXU&list=PLOYHgt8dIdoxx0Y5wzs7CFpmBzb40PaDo&index=1](https://www.youtube.com/watch?v=ErOzmh3BiXU&list=PLOYHgt8dIdoxx0Y5wzs7CFpmBzb40PaDo&index=1)

* NVitanovic - C++ basics and Simple Console Games - [https://www.youtube.com/watch?v=fGWbpl3H_OE&list=PLrjEQvEart7dezh2ObeI1S7L2YDZdIT9T&index=18](https://www.youtube.com/watch?v=fGWbpl3H_OE&list=PLrjEQvEart7dezh2ObeI1S7L2YDZdIT9T&index=18)

* javidx9 - C++ simple games and more - [https://www.youtube.com/watch?v=b6A4XHkTjs8](https://www.youtube.com/watch?v=b6A4XHkTjs8)

### Interesting Presentations
* GDC Ten Principles for Good Level Design - [https://www.youtube.com/watch?v=iNEe3KhMvXM](https://www.youtube.com/watch?v=iNEe3KhMvXM)

* GDC 2017 - Achieving High-Quality, Low-Cost Skin: An Environment Approach
[https://www.youtube.com/watch?v=qnxCcY0WDAk](https://www.youtube.com/watch?v=qnxCcY0WDAk)

* Cinematic Lighting Techniques | Part 1 - [https://www.youtube.com/watch?v=eZ5hpcn6tIM](https://www.youtube.com/watch?v=eZ5hpcn6tIM])

### Resources

* Sounds And Footage Paid - [https://footagecrate.com](https://footagecrate.com)

### MongoDB

* [Quick Start: How to Perform the CRUD Operations Using MongoDB & Node.js](https://www.youtube.com/watch?v=ayNI9Q84v8g&feature=emb_title)


