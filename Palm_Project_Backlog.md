# PROJECT PALM BACKLOG


# KNOWN BUGS

- rotate tool ( needs to check before rotation to avoid incorrect positions ).
- wall placable object can be placed on ladder ( because ladder is wall type, change it to other type but keep with same functions as wall has ).
- fix collision for walls ( So placable wall object can be placed correctly ).
- door when opens don't check if something is not going to block it( door might go through another door on other wall ).
- some windows can't be rotated.

# TO IMPROVE AND MAKE

- main menu ( with background etc ) + settings menus
- buildings menu
  - building add missing icons and make missing wall elements.
- upgrades menu
  - add missing icons, main upgrade remake ( with paint and upgrade tools ).
- round selection menu
  - upgrade graphics ( tweak colors )
- game menu ( main screen, popup questions windows ).
  - UI remake
- HUD info ( balance , tasks, )
- search functionality.

# part of newer systems to test

- wall modules figure out, with edge fixes for concrete walls( size: height, width, lenght).
- addable wall facade
- 0,75m foundation quads.

# house flipper 2 demo test info

- surprice unity instead of unreal (LOL)
- people review ( to many tools, changing them is frustrating )
- people review ( performance ).
- people review ( UI a little bit worst than one ).
- people review ( bad looking textures ).
- performance was better with less tasks.

# quick ideas
- glass walls like windows, no need for wall elements. modern style.
- glass foundation.

# Foliage
- for trees European Hornbeam Megascans
- mid size foliage European Hornbeam, Common Hazel Megascans

# TO DO
( tutorial time current: 3min build, 2min upgrade, 3min paint ).

- ( generally done but we will look for better solution ). ! fix loading issue for added meshes. 
  there is issues with loading shape upgrades ( they switch to base version ).


// general milestones
- ! cash ( proper calculation and prices etc. ).for upgrades.
- main menu settings windows look dev
- main game menu visual upgrade
- island look dev
- lighting dev
- descriptions for objects and objects menu cleanup ( use gpt ).
- sea upgrades
- few houses with bridges on island + welcome dialogue and first assignments etc.
( , introducing to agent and emails and jobs, design tasks for specific clients/quests, )

-  budget + reward research.
- dalszy upgrade dialogów.
- can finish task at 70% done. 
- check if we can use UpgradeType in materials datatable ( to determine on what type of objects can be used material ).
- check key for switich thru materials menu.

### TO DO
# UI jobs
- properties screen
- loading screen
- mail screen update
- upgrade menu update
- painting menu update

# tech stuff

-1
-2
-3
-4
-5
-6
-7
-8
- fix collisions for better object placement on top of them.

- PAUSED - need interface to get object that are on top of the another object. now we can remove only top objects but not if something is on that.
- plastic better material not flat. ( from asset pack ).
- for better patterns and wallpapers need some VTR(visual targets)
  will be also usefull for more furniture ideas( check also House Flipper 2 for patterns/wood/wallpapers).
- check in game patter tiling.
- check hotel renovator furnitures etc. (patterns).

- patterny płatne, lub z cloud adobe.
- paint menu needs generic update ( add ability to change paint color first ).


<!-- To Fix--> 
- check floorstripe for Asymetrical Walls Loading From Save.

23( research house flipper 2 and references for more object ideas and look dev).
- check difference floor 02 and floor 01 base blueprints.
 ( quick check suggest that floor objects 1 are big and can't be put on other objects,
   floor objects 02 are small and can be put on floor objects 1.).

- sun shutters snap behaviour - needs fixing.  
- pool corners detection for cross aligned pool squares.
<!--  --> 

<!-- Functionalities To Add Or Improvments--> 

- Disable Building ID Check - during testing assets etc we need to be able edit assets.
- beds fixes, patterns, colors etc.
- update all available upgrades on begin play ( after changes some level actors might not have them ).
- fix trace for placeable for ( trinagled walls + other elements thay u can find ).
- custom names for upgrades.( for shape upgrades )

- Waterslide functionality.
- Waterslide water improvments.

- Constructor, object display in menu ( 3d render target ).
- Constructor, spawn setting change ability. 

- frame rotation add.
- frame, some ai generated images. ( version with no white frame ).

- Painting sound.(paint splat.)
- Selecting, selecting multiple surfaces to upgrade.

- add editor settings ( update actor upgrades shape and paint ).
<!--  --> 

<!-- New Objects --> 

- lamele BP.
- one palm rug.
- palms pot plant.
- white tiles bathroom
- kran for new sink.
- folded towels stack.
- bathroom small items ( soap bottle, cup for teethbrusg etc., toilet paper.)
- bathroom sink texture.
- new cabinet livingroom.
- beds general improvments.
- Sunshade closed version.

- ! plenty new prototype assets ()
- Coconuts drink model.
- Rose leafs spawned as particles.
- Additional pillows to place on floor or sofa.
- few decoration elements to put on cabinets or shelfs.
- lamele.

- stolik kwadratowy.
- pufa do sofy.
<!--  --> 




- fix building foundation underwater ( underwater can't be delated ).
- half foundation ( add height adjustment).
- upgrade mission end screen.


- fix color palette reloading when switching materials for different upgrades and not different.
- wood mat upgrades correct order.
- unify rugs uvs scale, mesh scale and pattern scale. ( rugs needs to have correct size to match pattern size ).
- materials menu update documentation for materials tabs.
- after picking paint in other tabs upgrades material tabs resets to start selection.
- if we have color picked for one material and switching to another, the color for this first one is reseting( should keep this data ).
- find correct texel densities for different wood types /// wip - texturs finding and materials segregation

- check anchor for tutorial building and others ( error ).

- patterns support (added pattern, need to make proper fabric material setup. need to add pattern colors. need to update material apply for pattern for different upgrade types.).
- improved upgrade menu.


- saving windows ( leaving and saving game, fix window question visibility ).
- new saving indicator and saving window information.( check saving after leaving game. ).
- sofa


- morning glory make temp speedmodel.
- tropical grass ( check distance blending compare to old grass .)
- grass to sand transition research.
- more battlefield tropical map research ( overall look bushes trees palms compositions. ).
- wall planks 4 more tests. ( mask dirt, add some paint flakes ).

- reef speed model + deepth fade on reef stones.



- not selected any property and we still can click visit and popup question is showing.

// - half foundation not working ideally
   - halfwall not building ideally.If we build wall with half wall block there will be issue with texture tiling.

- upgrade menu switchable upgrades ( make sure they work ).
  1.( selecting back same material upgrade should not reset its settings ). 
  /// only currently used material is displaing color icon correctly other materials are reset to it's defaults because paint data is never saved anywhere. Ideally we would need to change all PaintUpgrades to PaintData struct.
  2.( accept upgrade variable is for gameplay manager to know that it should be pinged ).
  /// after accepting there is no upgrade application so accept variable might be not needed.

- UI look to rainbow six siege.


- done - adding same style to object is charging money when it should not.
- white textures wood painted ( for door, window, walls ) maybe also cream version and damaged paint.
- wall separator tiling texture.

- sunshades for windows.
- show on end quest profit,spend cash not only full received cash.

- fix floor objects 01,02 unify those.
- walls additional snap points.

- separator wall base.
- corners extend

- pedestal, toilet mesh fixes.
- pools fixes

- custom build collisions ( for example for sunshade ).
- upgrade menu check upgrades modulars for same selection. correct value for selected upgrades.

- ! half foundation is not updating facade.
- half floor detection code fixes.
- rotation for floor objects on grid.


- menus popup windows needs style upgrade.
- tasks list menu visual upgrade needed.
- saving text needs tweaking.

- walls when cancel upgrade disapear.(temporary wall elements switch on/off are disable).
- (tutorial fix message) message after tutorial is not sent because after loading next tutorial map savings are reset.
- floor stripe for half wall can't be build when other halfwall stripe is next to it ( its a building collision bug ).

- !upgrade previewing setting.
- !side task list colors.
- color palletes system ( basic is done, add patterns ).
- destroying buildings also needs to remove building objects from map.

TO ADD 

- move object with other on top of it.
- objects grid display.

- separator wall.
- fix for sunshade.
- water slide.

### bathroom stuff
- cabinet with sink 01
- cabinet with full sink on top of the cabinet 02 
- toilet 01
- bathtube 01
- cube toweles
- rolled towels
- shelf or something for towels.
- mirror
- small elements, soup container. paper stand or wall stick.

- placeble pillows.
- low table coffee.


- look for new solution for taking screenshot of the buildings(render target is bad).
- ( buying and selling properties ) you have to pick map and also docs ( land below building ).
- color pallet system.( colors needs tweaking and new UI for upgrades. make color pallets for painting walls).
- ! ( try using wall object as grid, will make simple to display on walls. ) grid display system.( for walls and floors test).



- floor stripes adjustments ( thickness lower than door/window frames ).

- check highlighting tiles(blue) when all object is highlighted (yellow)
- upgrade preview system documentation+ 
- !upgrade preview while reverting on some objects like wall, object is not completed.
- !upgrade preview prevent from selecting currently selected upgrade
- upgrade correct calculations.

- !before task check if it is not done by player already.
- !!!upgrade modular price add/remove etc.





- ! adding Required upgrade/paint update in task description at the end ( highlighted ).
- ! interactions fix owner ( that cause worng dispaly info sit instead of get up etc.)
- ! swiming fixes ( general , while swimming if I change building mode swiming is exiting ).
- ! walls materials after flipping. ( need some ideas ).
- ! screen icons for corners.


- island meshing for tutorial.(finishing palms etc.)
- ! turn off cast shadow and GI for moved objects.
- turn on and test DLSS
- ( stripes. corners adjustments.)
- corners + floor stripes adjustments



- bying and previewing as spawning ( check functionality . )
- finish roof straw etc.
- ! move stairs from the wall to it don't collide with trims.

// landscape steps
quick 5.3 nanite landscape test.
move displacement textures, 
  1. maya basemesh, scale, correct beach and reef, 
  2. imported as height map to UE5 landsacape, 
  3. manual ue5 adjustments.


- ! maybe remove outline from highlighted object effect in stencil for tutorial.

- ! separate tracing for edges and stripes ( sometimes on second floor edges are delated or flipped with second floor wall ).

- loading before starts ( so there is no streaming effect ).
- ! moving tool if trace is missing object creates wrong behaviour.
- limit camera object interaction, fix ownership after leaving object camera, bock other interactions while sitting in object mode.

- ceiling trims painting
- ceiling + trims detections fixes.
- roof basemesh lumen test.
- delate assets that are placed on the top of another when main is delated.
- lighting ( ue5 - play with skyatmosphere + skybox with clouds texutres )
- spawning object performance ( multiple check spawn ).
- DLSS test - implement for build.
- local grid mode?
- picking up rotation research and maybe some adjustments.
- midjourney menu designs/ logo designs
- ! chair adjustments for landscape.






# Quick TO DO
- add heightfield mesh for island.
- test ultra dynamic sky.
- water far mesh fix.

- palm bark fix
- new palm remake
- ground plants small
- reef base meshes
- underwater post process depth fix
- island layout new reef etc.
- sałata - one big krzak.
- foliage wind check for quixel wind.

- tutorial todo: block area for player.
- tutorial - blocking other options sometimes ( upgrades, paint etc ).
- display hint about keys.
- tutorial: upgrades, painting.

- roof sides ( vertical sides detection ).

- stat RHI ( performance check)
- cgaxis interesting furniture packs

- half roof need new support detection.
- roof system węzły for better detections( - 90 deg detection for roofs fix. collsion detection fix.)
- modular sofa ( other furnitures ).

- roof finish modules ( sides, half roof ).
- floor strip corners.
- ceiling bug when turning on/off ( ceiling matrial appear on floor ).
to fix trims and ceiling we need to check first if ceiling is right ( thickness and right height for stairs ). check stairs first.
check modular viking houses. cgpeers.

check roof border. not disapearing sometimes
- big - ( fixes pool )
- big - ceiling and trim connection systems.


- corners switch on/off.

- bushes and palm source files review.
- roofs bamboo geometry.
- walls upgrades checkups to prevent upgrades missmatch.
- offset beetween object and trace hit ( combined ).

- swimming pool spawn fixed height ( some offset to add ? ). Be aware of othe opcje snapping.

- vector transform in space.
- ceiling paint for trims or separate color changing ability.
- rock foundation

- swimming pool collisions on trim. to fix. after changing trim.
- finish all foundation tiles. ( check ceiling )
- roofs.

- research ( how in housefliper or sims floor and walls are painted / upgraded / changed ).
- check per component what material can be assign, probably new node for updating component.
- check available paint upgrades on material application for walls.


- roof side upgrades are blocked by triangle wall.
- organize icons.
- photomode fix with other opening windows behaviour.

- wall sides upgrades.
- component only highlight for paint tool ( as start ).
- trim auto corners.
- ceiling trim 4 side adjustments paint.
- walls component painting.
- full wall and frame wall loading issue.

- add wooden ladder.
- upgrade menu ( recognizing upgrade type and base on that creating shape selection or basic upgrade ).
- trim choosing different stuff

- floor texture tiling.
- ALL around modules fixes.
- trim for HalfFoundation.
- water ladder.
- upgrades relationships.
- half roofs. roof update fixed geometry ( and collision ?).
- wooden triangle wall finish.
- maya files cleanup.

- half wall rotation ( add check for blocking objects ).
- modules system update.
- to fix ( after adding window to wall frame can't add wall upgrade.)
- modular stuff wall for sloped wall.
- sloped wall flip checker.
- 4sided door ( not heigh enough to frame ).
- add only door frames ability.
- half foundation wood ( bug when near snapp and adding height adjusment , unable to build ).
- triangle wall wooden.
- Ceiling Improve.
- foundation triangle ( floor rotation not working ).


- object move together with other objects if placed at it.

run final complete along with end dialogue ( now it's playing after end dialogue ). resarch it also in other games.



( roof materials fix )
roof side system fix for wooden roof elements. 
roof upgrades checker + wooden side elements
add info about Q tool box ( so player will know how to delate objects , in case he create wrong one )
building simulator - check tutorial game.

"walls modular, now can not be rotated while being snapped to floor".
check roof for tutorial.
not allowing player to spawn wrong object at wrong place


icons menu upgrade
icons walls
roofs.


- roofs setup for new workflow
- cost during upgrading elements
- grid system
- walls system ue5



- if upgrade info open we can change building mode ( we should not be able to do that, it cause problems )
- fix wonkines of menu ( weird shuttering )
- remmember opened tabs in upgrade menu?
- show or hide upgrades that are dependent on others ( like floor strip that requires facade wall or inside wall etc ).



- test few icons scales in different resolution and different DPI to se how they look.
- keys mapping scale down.

- few plants and furnitures still needs to use our master material.
- sold img on details panel.
- remove button after selling property.


- fishes
- reef
- fix normals for waves ( space normals ).
- caustics on rocks, and other stuff like pillars etc.
- unload level after renting or selling currently visited level. ( need some warning display ).



# DONE
- DONE - double door sliding v2 ( that is detecting doors ).
- Fixed - fix floor stripes for openframeside.
- Fixed - Fix was needed for adding FloorStripe To Sloped Walls And Also Non Symetrical Walls Like FrameOpenSide.
- Fixed - FrameOpenSide wall detection fixed ( used DetectionHelper ).
- DONE - Wall Frame Open Side - add rotation ability like on triangle wall to switch side during build.
- DONE - trace for placeable needs fixing for walls placed 90deg to delated wall.
- DONE - marble black 01.
- DONE - Upgrade System, issues when 3 or 4 elements can be upgraded (no place for storing upgrades, no good names for those upgrades types.)
  Would be probably better to create one available upgrades array listed by upgrade type. 
- DONE - new double door wooden.
- Fixed ( glass material now have proper outline while selecting ).
- DONE ( modified 'check build collision' function, and 'remove build collision' function ) - custom build collisions
- DONE ( texture added ) - white wood stuff for furniture.
- DONE ( make socket transforms function was overriten ) - ! pool ladder fix ( should not build on hidden walls ). 
  // we need dynamically add and remove tag for pool socket.
- DONE ( added additional Z check for Check Floor Function )- check for height between floor socket and placed actor location ( to avoid puting object on unwanted places ).
  (test on armchair and bed).

- done - delate objects that are on top on the another one.
- fixed( needed to unchage use spawn settings when moving object ) moved object with shape upgrades is loosing its paint. 
- done - we can use black and white pattern and also FullyColor patterns ( added new variable in STR_PatternTexture Called ).
- done - images fix material ID, fix wood frame.
- ( fixed | may need future revistions ) fixed multiple unique textures 
- fixed - if we switch material the color palette is not reseting for newly selected material. ( we needed to reload palette after calling select event ).
- done - now we can click again on selected pattern to turn off patterns completely.
- done - added support for setting pattern colors.
- done - added basic support for finding objects ( getObject )(need to also create inventory for them).
- done - (temporary windows added ) add buy window,sell window, and questions.
- fixed - dialogue sound and dialogue text is now correcrtdly paused when pausing game.
- done - adjust grid display on halfwall Frame.
- fixed - (16.08.2024) - fix halfedge curtain position so we can have corner curtains. ( add scroll position adjustment ).
- fixed!(14.08.2024) - curtain end mesh added + code fix so it displays correctly.
- fixed- pool dirt now display properly. fixed second uv set.
- fixed- (11.08.2024) double door openwork was able to build on themselfs ( fixed door reference variable ).
- done - (11.08.2024) fixed fan snap behaviour, build status was overitten and is checking support at the end, any other methods failed since system is checking stuff not in order we need. ( probably would be nice to upgrade the whole system for checking sockets, collisions, supports etc.).
- done - (10.08.2024)addable frames for door,window 
- done - add one small palm.
- done - concrete foundation dirt tiling fix
- done - ubrella lost small ring on top. add it back.
- done - temp model sałata big.
- done - palm trunks bottom roots.
- fixed!(30.07.2024)fix simple tree from jungle pack.
- fixed!(13.07.2024)(added function that checks if upgrade already added )! roof triangle had 2000 current upgrades ( someting is adding them uncorrectly ).
- done!(13.07.2024)halfwide roof for halfroof ( for straw roofs?)
- fixed!(13.07.2024)( changed text variable to multiline ) tutorial task info text is not wrapping correctly.
- fixed! roof snapping to half foundation.
- fixed! make half window narrow.
- fixed! check if halfwall can be flipped.
- fixed! half wall V should attach curtain edge when flipped.
- fixed! half wall V should delate curtain edge when delated.
- curtains edges done.
- curtains standalone version done.
- bridge ladder detection fixed.
- done - added ability to paint wall corners
- fixed - floor stripe can now be added to half wall ( fixed build collisions by adding adjustment build collision function node).
- fixed - potential bug when playing again quest and after saving it we can't click continue ( Only Play Again is visible ). 
  Probably It  is still set to complete after previous game. 
  /// in gameinstance function 'set quest started' set quest also to incomplete.
  
- fixed - double door + stripes ( fix some smaller missaligments )
- done - grid now is changed to 20cm ( grid15 material ) in order to match wall location better.
- done - curtains optimization pass.
- done - bathroom icon
- done - color palettes switching buttons fix ( select right palette at material click ).
- fixed - ( cause of this was actor added to Action on End Task ). !wall painting is blocking gameplay.( checking anchors ).
- fixed - wooden wall upgrade apear on floorstripes upgrades list ( should not be there ).
- fixed - ( some points were rotated ) grid fix in the middle of the full wall there is some isuess and object disaper.
- fixed - isQuestObject when moving is setting variable to true incorrectly.
- fixed -! menu wallpaper background , fix barevisible watermark.
- fixed - when trace hits other floor object our currect build object is disapearing ( should show up on this target object with can't build look ).
- fixed - !!! full wall window can be added to doorframe and other wall types.( check this ).
- done - grid documentation.
- done - !changing tool via round menu when camera is active does not disable camera tool.
- done - after move hide grid.
- done - wall object correct snap behaviour.
- done - grid display when turning grid on/off works now correct.
- done - grid points setup for all wall and floor parts, grid display works.
- done - in gameplay manager paint upgrade can be changed to new system with palette use.
- done - grid correct geometry for walls and floors added.
- done - extented to more than one actor - grid display.
- done ( it works but need to press update or snap to ) sloped wall floor stripe ( making it update on manual build ).
- done ( it works but need to press update or snap to ) manual build to target ( floor stripes/edges).
- done - half wall floorstripes - fix
- done - all usable assets ( check paint settings in datatables so we can remove old paint variable ).
- fix door/doubledoor/4sided door frames. ( did on same blueprint + added specialcommand setting "disableinteraction" for outerframes ).
- done - half wall fix for sides and materials ( like on the triangle wall ).
- done - checking save game for quest ( to enable or disable buttons in job Messages ). need to add another stuff to track quest started.
- done - wood fine 01 textures ( fix by offsetting and fixind edges ).
- done - !fix slide door previews in viewport and 4sided door previews in editor. Not all door elements are displaying.(maybe add additional mesh array to object settings).
- done - !icon for concrete half foundation.
- done - screenshot loading at building manager start.
- done - half foundation size calculation.
- can't reproduce anymore - !critical! painting walls during task cause next task not showing up.
- done - ( check ) fix unwanted behaviour ( spawn object from other task cause some trouble ).
- done - !when adding half foundation fix added size. now it's adding to size 9 instead of 4.
- done - buy, sell, destroy works.
- done - actors to remove are now highlighted and no other during quests are now highlighed( might change in future).
- done - removing wrong actors for remove task is fixed.
- done - basic color palette done.
- fix - fliping bridges ( maybe other objects to ) dont transfer some variables and we can't edit it after flipping.
- done - !updating nearby objects after settingup objects (removing objects that will be needed to by spawn in quest).
- done - after moving quest floor object variable IsQuestID was not transfered with it. Added GetIsQuestObject?_BPI in Durability BPI to get this variable for moved object.

- done - ( building saving to own slot ) ! each building must be saved to own save slot. 
- done - add leave property check saving it before leave.
- done - ownership checking.
- done - block painting for objects that are not allowed.
- done - System For Building Quest ( Instead of putting manually position for buildings we 
should have full building done and only pick actors that later will be removed). System will allow us easily repostion all buildings with quests. 
- done - task list focus to current active tasks.
- fixed - floor stripes corners for half wall now works with correct position ( just pin bug ).
- fixed - saving icon indicator. ( also during saving maybe we need to block player from other actions like saving again or pressing something ? )
- done - houses position ( tool for editor to snap whole house to other place like bridge ).
- done - leaving quest or starting ( beacause pause and unpause there is sound hear right before loading map ). ( sound fade in fade out ).
 { nosie while exiting map was fixed by pausing all ambient audio manually insted of stoping it. Added Fade In For ambient sounds on spawn ( in player controller .) }
- done - ! exit/leave/cancel task - question window documentation.
- done - saving and not saving for leave quest window.
- done - leave current Job button ( in task list ).
- !input issue after loading level M/T other menu turn ON keys not working with first press. ( reason for this was not setting Pause Variable to False in gameinstance ).
- done - ! wall sloped fix for flipping TAGs -> flipping materials for wall sides ( wall tiles ).
- fix - email list overflow fix.
- fix - ( fan set to ceiling object type, ceiling and foundation have 90deg + veryfication so 90/180/0/270 will be accepted) fan detection for location 90deg all angels.
- fix - added revert shape function ( for reverting to base floorstripe shape when target actor not found. Ideally would be to do this once and only when attaching not posible or somtheing more optimal to not set this mesh on tick ).(revert shape node is called in -> player controller -> building component -> update building position ).
- fix - building incorrect buildings during tutorial fixed.
- fix - object after move needs to report to GM.
- fix - floor stripes now can't be built one on the top of another.
- done - make sure tutorial gameplay is not saving ( we don't want tutorial progress ). ( prevent buildings and quest from saving ).
- fixed - single task handling by BP_GameplayManager
- fixed - task widget now displays number of task correctly after continuing game from save.
- done - !optimize bp_quest find buildings. ( add probably find buildings once in player controller and pass them to bp_quest)
- fix - display only holograms that are not completed.
- fix - fixed object saving ( now objects that are holograms can't be saved ).
- done - task list fix bugs ( numbers updates ). // ( 6.01.2024 ) now task list in building manager are updating, multitask array in gameplay manager is storing all info.
- fix - loading next quest on the same map not working. ( quest was not set ).
- fixed - bug - windows can be added to open wall. ( temporary fix ).
- fixed - (03.01.2024)proper loading upgrades for foundations ( there is UpdateMoudlar and Then LoadModular upgrades functions and they we forget to call this last one correctly ).
- fixed - wrapping in task list.
- done - propertyareaVolume is done. It has buildingID related to building on this area. During object spawn, building system is checking if overlaps with this volume and is checking for building ID and currentTaskBuilding ID.
- fix - folded door fix ( added check only frame function).
- done - added search bar for finding objects.
- fix - bad calculation was caused by not calculating upgrade cost in gameplay manager for buildings spawned (in this case it was Wooden_Wall).
- fix - bad values calculation after removing floorstrip and edge was caused by Load Modular Element Parent Node.
- done - now after applying paint upgrade it removes money and display change. This functionality was missing. ( need to do the same with modular upgrades.).( 23.12.2023)
- fixed - fixed floor stripes update on Open Wall Frame ( adjusted trace location for floor stripes ).
- fixed - fixed floor stripe could not be added when slided door was in wall ( door collision was blocking it ).
- done - if player is already in goal location game can recognize it and finish automatically Go TO Location Task.
- done - now disable icons from constructor are invsible for some time.
- done - ! loading place on the map. ( now you can set player start in location in Task ).
- fixed - dialogue warnings.
- fixed - after moving object during task it was loosing it's ID ( now its correctly added from object that was picked up). (16.12.2023).
- fixed - holograms issue, during gameplay they were not updating geometry ( fix was simple, only roof were blocked by it's built variable before update function, other stuff was updating correctly ).
- done - task icon ( replacing stats icon ).
- done - fixed Gina avatar, neck height and ability to change avatars easily based on datatables.
- done - added gameplay upgrade with edges, door, windows.
- done - updated floor stripes and edges manual build ( by adding manual build check variable). Allows floorstripes and edges to switch side but only in manual building in editor. ( 07.12 ).
- done - function check paint upgrade and check paint manual in gameplaymanager was updated ( previous version did not support multitasks ). I previously 
  was imposible to finish task and count completed elements correctly. ( 01.12.2023 ).
- done - now anchor points can populate itself to correct tasks ( 29.11.2023 ).
- done - added anchor points in Task Structures.
- done - fixed enhanced input for building manager navigation menu Q and E.
- done - added task ( remove object ) ability to remove gameplay objects.
- done - added ability to spawn objects in volume range ( instead of exact location or random location ).
- fixed - while painting manually ( auto paint ) there is an error when all elements of the objects are completed because objects are no longer stored in variable.
- done - ! 90deg check for foundation while spawning in gameplay manager.( only for quad foundations.)
- done - fixed pool and foundation detection for trims. ( better position detection ).
- done - buildings now have IDs so they can be locked for specific gameplay. ( if building mode selected it is not locked ).
- done - ladder now flipps with bridge bend. 
- done - improved water ladder and ladder functionality ( can now detect he bottom height and diable ladder if it is to short ).
- done - ! swimminig ( add in controller diving bool variable to be able to swim up and down ).
- fixed - water footsteps ( water body ocean now Is excluded from footsteps tracing ).
- done - ! gameplaymanager ( add multiple task check ability, multiple task list ).
- done - counter for multiple tasks works , completed tasks change style ( need to fix actor spawn checking location in player controller 
so we can make task spawn actor where ever u want available ).
- fixed - icons hilighting bug ( multiple icons was highlighted).
- fixed - fliping objects is no longer available in tutorial.
- done - saving works ( now add restore defaults and adjust mouse speed slider ).
- done - bridge textures update fin.
- done - grab icon and flip icon.
- f
inish bridges functionality ( no is cheking collision and support before flip ).
- !facade naming fix.
- fixed - ! painting fix if not possible info is flickering.
- fixed - ! object moving material is gone.
- fix - it don't calculate changes now - shape upgrades don't add/remove money. ( acatually it adds and removes the same amount so it's not changing ).
- done-task tips text , pick color for text.
- done - ! wheel menu visual upgrade.
- fix - reverted to old settings - fix triangle wall corners
- done - tutorial better task info look.
- fixed - red and green highlight for moving objects works now.
- fixed - move tool now work like at the past.
- done - improve radial menu Look.
- done - highlight icons don't work when upgrade or paint menu is open ( it's not refreshing ).
- fixed - roof can be build on roof fix it. ( problem during tutorial was that holorgram was also roofs ).
- done - tutorial block upgrading wrong objects or applying paint to wrong objects.
- done - blocking uncorrect upgrade modular.
- done- ! curtains base.
- fixed for now - borders fixes for roofs.
- fixed-! unnecessary row in menu empty.
- fixed-! buildings values are calculated wrong ( without updates price after loading game ).
- fixed - roof upgrades are not adding to building worth after build.
- fixed ! tutorial balance setup, tutorial property worth fixed.
- fixed by epic - epic solved probably ! nanite all elements and check outline for selection.
- done - when saving sometimes error about lost references in windows and door ( about wall reference ).
- done - half wall fix for stripes and edges.
- fixed-edge is blocked by windows sloped.
- done-triangle wall corners control
- done-floor stripes for sloped wall
- not needed at the moment - sloped wall rotation ( checking collision ).
- done - half wall flipping ( checking collision ).
- done - settings menu background stripes tests.
- done - stripes for half wall.
- fixed - ! roof borders fixes for geometry.
- done - roof sides ( loading saving geometry tests ).
- done-! floor strip corners/wall edges ( rethink system ). comunication beetween them.
- done - added paintable sloped wall and triangle wall.
- done -! check roof detection trace position( sometimes border is wrongly displayed ).
- fixed -! roof corners repetitve texture fix by making leftside mesh.
- done- !roof border detection fixes needed.( when roof is in same directionp[rotation] it can be 180 or -180 so we need to check for absolute rotation values ).
- done-! edge cant be added when roof is added to wall( collision is blocking it).
- done - after delating wall we delate also stripes and edges.
- fixed - get current resolution in graphics menu ( not working on build version ).
- fixed - !edge can't be added to door wall
- done - addable floor strip/trims 
- done - !remove edge and floorstripes from Wall Module ( But Make A backup for Wall Modules With those elements ).
- done - !add trace for edge system - propagate floor stripes to edges system
- fixed - was just wrong upgrade assigned ! foundation fix geometry for upgrade one facade.
- fixed - ! roof tiling geometry fixes inside map 4 .
- done - ! price divider added for floors and walls.
- done - ! highlight removal for tiles after completing each tile.
- ! sides recognition for tutorial wall paint.
- fixed -! highlighting correct upgrades ( some only in constructor some only in upgrade menu, or paint menu.)
- done - ! highlight asset for upgrade paint ( post process stencil outline etc. )
- highlight floor/main/ tab in upgrades.
- ! curtains materials.
- ! rug correct master matrial.
- ! sofa, table, optimisation.
- ! sied half roof mesh.
- ! half roof smooth corners
- ! half roof side roof detection ( needs fixes )
- done - ! ceiling trims base / extented.
- done - p should open properties tab, while ESC should open system menu with save settings.
- fixe - fix fan ceiling.
- fixed-! material assigning checkup.
- done - make carpet always ignored by objects.
- done - corners for wall top edge functionality ( same can by apply for floor stripes ).
- done - ! pickup object rotation fix ( building component update overrides default spawn rotation ).
- done - ! fix picking object from distance , it breakes, sets object to center.
- fixed right mouse button behaviour while falling.
- done - prevent building in wrong place
- done - highlight building icon in menu for Tutorial.
- fixed - water caustics tiling from above
- done - walls split to stripes.
- done - roofs ( half roof version triangle functionality).
- fixed - continuous and one click paint fixed.
- done - roof short version auto detection.
- done - trims functionality.
- done - reimport straw for roof and add 
- done - corners inside for walls.
- done - sloped wall window geometry
- done - foundations can be now easily place on water( Added cast to wateroceanactor to validate water )
- done - walls corner fix system.
- done - decking floor.
- done - photomode frame.
- done - object highlight still visible when switching to PhotoMode.
- fixed - floor rotation fixes, rotation on floor fix.
- done - component tile selection highlight fix.
- done - fix for half stairs on swimming pool.
- done - ladders for water, ceiling, pool inside and outside. use stairs sockets.
- done - paint upgrades fix names display.
- done - display Z change on HUD.
- done - split floor to tiles or to separate object at first.
- done - override tile materials when selecting upgrade floor paint from upgrade menu.
- done - door frames separate icons and menu for door and windows.
- fixed - floor strip loading correct version fixed.
- done - add second stripe type.
- done - floor stripe now can be added as separate blueprint object, not as an upgrade component.
- done - trim fix uvs. ( flip all uvs for ceiling trim.)
- done - roof fix to match triangle wall.
- fixed - window full ( and fabric ) wall can't be added now when certain upgrades are visible.
- Done - system for trims for floors and ceilings.
- fixed - roof snaped to foundation has different height position than snapped to sloped wall.
- done - roof support desk missing collision.
- done - add shift run ability , tweak speed.
- done - right mouse click when (mallet,building) menu was opened is fixes
- done - test write with styles now in dialogue box.
- done - block foundation rotation ability ( only floor can be rotated ).
- done - number in task list fixing and counting proper objects.
- done - import island landscape curve from old project.
- done - loading objects on maps that were saved on different map
- done - paint upgrades inside object settings should be separated into specific element paint upgrade ( this will remove need of duplicating materials inside paints list ). 
( so we might have construction paint upgrades, paint upgrades floor and so on.)
- done - info display in correct location + price update
- done - bind new smaller upgrade icons
- done - refresh update upgrade menu after applying upgrade
- done - fix movement after upgrade
- done - fix empty icon for upgrades
- done - scrollable upgrade menu
- done - if no upgrades add empty slots.
- done - don't close upgrade window after upgrade.
- done - correct properties nuber ( not counting sold properties )
- done - rotate floor in construction mode ( for editor )
- done - waves
- done - transition beetween properties ( better will be to ask if we want to save changes , we might acidentally delate something and not knowing that it was saved ).

- done - if you exit to pause menu it takes size m2 data from game instance ( save data ) not from real time data from the scene - update size m2
- done - taking data for m2 from save game instead from current data.
- done - switching beetween levels needs saving ( currently might not save all needed data ). Ask to save before visiting new level.
- done - creating new property while visiting other one( ask to save and exits current level ).
- done - after renting or selling current property leave current property.
- done - saving after exit to main menu don't work
- done - fade in na levelu
- done - add empty slots not working. 
- done - reset current slot after exiting to main menu. 
- done - no underwater post process in build.
- done - can't visit if rented. can't destroy if rented.
- done - After creating new game. Images from previous one are still loading to created property slots. Delate all images on new game.


- DONE - basic wall sides ( Corners ) detection system ( part 1).
- DONE - foundation sides system completed. 
- DONE - roof bottom fixed mapping for all roof elements.
- DONE - short roof upgrade don't remove money and don't add to property worth.
- DONE - add round selection toolbox
- FIXED - ladder can't place on second floor because of bad collision.
- DONE - fix placable object so it can be placed on different shelfes ( use placable wall object ).
- DONE - placable objects allow to put anywhere where the floor setting is on and collisions are correct. ( put on multiple shelfes needs fixes ).( use wall placable object settings for this ).
- DONE - wall placed objects 
- FIXED - wall placable object delate close objects after being delated.
- DONE - upgrade paint menu for shape upgrades
- DONE - introduce upgrades colors.
- DONE - glass windows ( with also opening functionality )
- DONE - put object on trinagle floor ( fixed by recreating triangle foundation from scrach ).
- DONE - ability to connect half roof to walls
- DONE - 4side door fixes ( moved to attached object ).
- DONE - ! window for wall ( make it to be attached object ). glass windows for wall ( need only fixes - use only on empty wall ).
- DONE - ( added barrier as and Modular upgrade to frame ) barrier to wall construction ( figure out if can be added ).
- DONE - roofs additional length fixes
- DONE - fix move tool ( materials setup is destroyed while moving ).
- DONE - window fabric ( check if other object is blocking, now it can be added even on window )
- DONE - sroda - compile and check if material instances are saving correctly.
- DONE ( new material apply code ) - Create Material Instance At spawn for objects ( to be able to apply current paint upgrade ) + paint upgrade for other static    
       meshes inside of BP ( override default settings )
- FIX - triangle fundament, pillar position.
- DONE - texture upgrades ( or rather full material ),upgrades icons,empty button
- DONE - upgrades ( highlight currently used upgrade, don't allow to apply )
- Done - table,armchair, singlebed
- DONE - paint for construction
- DONE - 4wing door 
- DONE - roof collision modification ( for better walking on the attic, and ability to add stairs (( now it's blocked ))
- DONE - add empty upgrade buttons
- DONE - ladder functionality ( location detection )
- DONE - rotation saving for doors not working
- DONE - crouching
- DONE - make 'allow move' settings ( not all objects shoudl be able to move )
- DONE - stairs bamboo simple
- FIX - footsteps separate for SFX audio controls
- DONE - rotate floor texture ( planks )
- DONE - windows fixes
- DONE - question quit game
- DONE - getting money back on object destroy
- outline FIX - for object not visible after completing MOVE or Build while aiming at same target building object.
- mouse controls done
- graphic settings done
- DONE - fix for object can't be build when player was stanting on initial object position.
- DONE - fix moving CANCEL 
- FIXED - bug ( while building and hovering over other objects- outline for object is blinking while it should be not visible ).
- FIXED - check moving one object after another, looks liek old moved object is remembered during second object moving.
- FIXED - fix after adding another floor and moving object that was one the floor it disapear after placing.
- DONE - fix collision while moving objects and during building ( deactivate collision after building instead of delate ).
- (move and rotate) picking objects and positioning them
- outline debug 
- outline for update,remove,rotate mode.
- continue game should not appear if we don't have save 'game'
- caustics water (fake) https://www.youtube.com/watch?v=9z6EMsoqLDY
- building cost calculating
- roof upgrades ( sides )
- bamboo fundaments
- buildings list ( display estimated worth )
- getting building value for buildings list
- getting money back when removing object
- saving scene to save slot from menu
- windows fix
- roof triangle
- saving one building and one map to one game save one slot.
- temp fix - Shadows Leaking 
- plants and trees placement
- Was already In Place "" placing smaller objects / furnitures etc. 
- interactive objects fixes ( windows rollets )
- color upgrades
- loading scene from save menu