# move pivot point to first point of the curve

```python
import maya.cmds as mc
curves = mc.ls(sl=True)

for i in curves:

	shape = mc.listRelatives(i, shapes=True)
	rootPos = mc.xform(shape[0]+'.cv[0]',q=1, ws=1, t=1)
	mc.move(rootPos[0], rootPos[1], rootPos[2], i+'.scalePivot', rpr=1)
	mc.move(rootPos[0], rootPos[1], rootPos[2], i+'.rotatePivot', rpr=1)
```

# select correct curve point

```python
import maya.cmds as cmds
curves = mc.ls(sl=True)
cmds.select(cl=1)
for i in curves:

      cmds.select(i+'.cv[5]', tgl=1)
      
```