# Motivation

### Tai Lopez
* [Tai Lopez on Why Hard Work Isn't Enough](https://www.youtube.com/watch?v=pQLG4U5A68k) - YouTube MotivationGrid Tai Lopez

### Rob Gryn
* [Morning habits](https://www.youtube.com/watch?v=olIFmAC9qvM) - Morning habits. How to set yourself up for success. Emotions follow actions.

### Simon Sinek
* [How great leaders inspire action](https://www.ted.com/talks/simon_sinek_how_great_leaders_inspire_action) - Simon Sinek - TED speech about leaders.

### Dan Peña
* [Maximum Business Potential - Event with Dan Peña](https://www.youtube.com/watch?time_continue=5204&v=-fzFb2S0kl8) - Dan 2h speech at Michael Pilarczyk's event.

### Dopamine Detox
* [Dopamine detox, be more productive etc...](https://www.youtube.com/watch?v=_vcGF3HqZXg) - dopamine detox

### Productivity (begining work might create stress, brain have to adjust to be focused, deadline on task might increase productivity other wise you will likely procrastinate).
* [Change Your Brain: Neuroscientist Dr. Andrew Huberman](https://www.youtube.com/watch?v=SwQhKFMxmDY)

### Alex Becker's Channel 
* [Why Rich People Have The Worst Depression... 'finding fullfilment in work rather than money'](https://www.youtube.com/watch?v=cyyfUinl7vM)

* [focusing on product/servis quality because there are plenty of the same businesses ( they all will be gone and only few will stay, like rockefeller and oil refineries )](https://www.youtube.com/watch?v=r-0FTT0VuHw)

