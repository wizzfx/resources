# Unreal Engine 4 C++ Etc.

### Learning ( Tutorials )
* [https://www.packtpub.com](https://www.packtpub.com) - Very good paid video tutorials for coding

* [UnrealGaimeDev](https://www.youtube.com/channel/UCRnPBe1tJpXA0lccx_U1mww) - Blueprint scripting excellent channel

* [Mathew Wadstein](https://www.youtube.com/channel/UCOVfF7PfLbRdVEm0hONTrNQ) - Best UE4 channel, blueprint scripting and other code related stuff.




### Stuff for UE4
* UnrealVS - plugin for unreal that can be found in Unreal Engine files.
* Editor symbols for debugging ( UE installation Options )- symbols to allow debugging C++ in the editor
* Visual Assist (plugin for Visual Studio, code color scheme etc.)

* Game Mode
* |
*  -- AI Controller --  Pawn ( possess pawn )
*  -- Player Controller -- Pawn ( possess pawn )
*     |
*     |
*         ( contains HUD, Input, Player Camera Manager )
*      -- HUD
*      -- Input
*      -- Player Camera Manager

### CPP Stuff
- bool, float, double, int, char (single), strings (whole series of characters)
- arrays
- methods and functions
- object oriented programming (OOP)
- 3 keyword control:
  - public (access for any class)
  - protected (access that variable or call that function, only classes that inherit from the parent class)
  - private (call it within itself, access only for function or class that have it)
- classes (private) and structs (public)
- ->access operator, * pointers
- in .h file we have definitions

### Object classes
- UStruct
- UObject
- UActorComponent
- AActor  

- UObject -> AActor -> APawn -> ACharacter
- UObject -> UActorComponent -> USceneComponent

### Naming Convention (prefixes)
- A Actors - AController
- U Objects - USceneComponent
- E Enums - EFortificationType
- I Interface classes - IAbilitySystemInterface
- T Template classes - TArray
- S Widget (Slate UI) - SButton
- F Struct - FString

### .gitignore for UE4
- Binaries, Build, DerivedDateCache, Intermediate, Saved   
  (we can delate them, they will be rebuild)
- you can also delate from project files "projectname.VC.db"

# flash/head light position
(X=-44.481750,Y=2.000011,Z=41.108063)
(Pitch=-15.000000,Yaw=0.000183,Roll=0.000000)
